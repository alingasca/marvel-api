export const HOME = '/';
export const FAVORITES = '/favorites';
export const PROFILE = '/profile';
export const INFO = '/info';