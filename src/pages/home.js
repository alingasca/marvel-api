/* eslint-disable no-unused-vars */
import React from 'react';
import { HeaderContainer } from '../containers/header';
import { ContentContainer } from '../containers/content';
import { FooterContainer } from '../containers/footer';

export default function Home() {	
	return (
		<>
			<HeaderContainer />
			<ContentContainer />
			<FooterContainer/>
		</>
	);
}
			

