import React from 'react';
import { HeaderContainer } from '../containers/header';
import { ContentContainer } from '../containers/content';

export default function Favorites() {
	return (<>
		<HeaderContainer />
		<ContentContainer />
	</>);
}
