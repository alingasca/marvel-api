import { createGlobalStyle } from 'styled-components';

export const GlobalStyles = createGlobalStyle`
  html, body {
  font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;
  -webkit-font-smoothing: antialiased;
  -moz-osx-font-smoothing: grayscale;
  background-color: #000000;
  color: #333333;
  font-size: 16px;
  margin: 0;
  -webkit-overflow-x: hidden;
  -moz-osx-overflow-x: hidden;
  overflow-x: hidden;
  a {
    justify-content: center;
    text-decoration: none;
    color: inherit;
    color: unset;
  }
  #index {
    .cardEdit {
      display: none;
    }
  }
  .expand {
    position: fixed;
    top: 0;
    margin: 0;
    max-width: 100%;
    max-height: 100%;
    bottom: 0;
    left: 0;
    right: 0;
    border-radius: 0;
    height: 100%;
    z-index: 999;
    background-color: #fff;
    overflow-y: auto;
    pointer-events: auto;
    left: auto;
    /*transform: translateX(0px) translateY(calc(-580px + 2em)) scaleX(1) scaleY(1) translateZ(0px);*/
    box-shadow: rgba(0, 0, 0, 0.75) 0px 3px 10px;
    opacity: 1;
    transform-origin: center center;

    &:hover {
      transform: scale(1)
    }

    .closeCardModal {
      display: block;
      position: absolute;
      z-index: 9999999;
      background-color: grey;
      border-radius: 50%;
      width: 40px;
      height: 40px;
      right: 35px;
      top: 35px;
    }

    .expandCardModal {
      display: none;
    }

    .cardThumbnail {
      border-radius: 0;
      overflow-y: auto;
      pointer-events: auto;
      margin-bottom: 50px;
      background-color: #fff;
    }

    .cardImgFrame {
      position: relative;
      border-radius: 0;

      .cardImage {
        width: 100%;
        height: auto;
        max-height: 100%;
        margin: 0 auto;
        border-radius: 0;

        @media (min-width: 640px) {
          max-width: 300px;
        }

        @media (min-width: 992px) {
          max-width: 400px;
          margin: 0;
          border-radius:2%;
          position: absolute;
          top: 50%;
          left: 50%;
          -ms-transform: translate(-50%, -50%);
          transform: translate(-50%, -50%);
        }

        @media (min-width: 1200px) {
          max-width: 100%;
          border-radius: 0;
        }
      }

    }
    .cardContent {
      background-color: #fff;

      @media (min-width: 992px) {
          margin-top: 12%;
      }
      .contentWrapper {
        text-align: center;
        max-width: 92%;
        display: block;
        margin: 0 auto;
        padding-top: 25px;

        @media (min-width: 992px) {
          padding: 8px;
          align-items: center;
          width: 100%;
          max-width: 80%;
          margin: 0 auto;
          padding-top: 0;
        }
        
        h2 {
          font-size: 1.9em;
          background-color: #fff;
          width: 90%%;

          @media (min-width: 768px) {
            font-size: 2em;
          }

          @media (min-width: 992px) {
            font-size: 2.2em;
          }
        }
        time {
          font-size: 1em;
          background-color: #fff;
          width: 100%;

          @media (min-width: 992px) {
            font-size: 0.9em;
            margin-top: 0px;
          }
        }
      }
    }
    .cardContent, .cardImgFrame  {

      @media (min-width: 992px) {
          width: 50%;
          float: left;
          height: 100vh;
      }
    }
    .actions {
      display: none;
    }
    .descriptionWrapper {
      display:block;
    }
    .cardEdit {
     display: block;
    }
  }
  #favorites {
    .expand {
      .actions{
      display: block;
      visibility: hidden;
    }
    }
    .addToFavorites {
      visibility: hidden;
    }
    .expandCardModal{
      float: right;
    }
  }
  
}`;