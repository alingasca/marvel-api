import styled from 'styled-components/macro';

export const PaginationContainer = styled.div`
  display: flex;
  margin: 0px;
  height: 60px;
  padding: 18px 0;
  justify-content: space-between;
  align-items: center;
  z-index: 2;
  position: fixed;
  bottom: 0;
  width: 100%;
  margin: 0 auto;
  background-color: #000000;
`;

export const Frame = styled.nav`
  display: flex;
  justify-content: center;
  margin: 0 auto;
`;

export const Ul = styled.ul`
  list-style: none;
  padding: 0px;
  display: flex;

`;

export const Li = styled.li`
  background: #252525;
  margin-right: 4px;
  border-radius: 4px;
  font-family: sans-serif;
  color: #fff;
  font-size: 1.1rem;
  min-width: 18px;
  text-align: center;
  user-select: none;
  padding: 7px 8px;
  margin-right: 6px;
  border-radius: 4px;
 
  @media (min-width: 640px) {
    padding: 10px 15px;
    margin-right: 6px;
    border-radius: 4px;
    min-width: 20px;
  }

  &:last-child{
    margin-right: 0px;
  }
  &:not(.active):not(.disabled){
    cursor: pointer;
  }
  &:not(.active):not(.disabled):hover{
    background: #4a4a4a;
  }
  &:not(.active):not(.disabled):active{
     background: #252525;
  }
  &.active{
    background: #727272;
  }
  &.disabled{
    //background: #252525;
    pointer-events: none;
    display: none;
  }
`;
