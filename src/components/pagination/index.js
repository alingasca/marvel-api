/* eslint-disable react/prop-types */
import React from 'react';
import PropTypes from 'prop-types';

import { 
	PaginationContainer,
	Frame,
	Ul,
	Li
} from './styles/pagination';


export default function Pagination({ children, ...restProps }) {
	return (
		<PaginationContainer {...restProps}>
			{children}
		</PaginationContainer>
	);
}

Pagination.Frame = function PaginationFrame({ children, ...restProps }) {
	return <Frame {...restProps}>{children}</Frame>;
};

Pagination.Ul = function PaginationUl({ children, ...restProps }) {
	return <Ul {...restProps}>{children}</Ul>;
};

Pagination.Li = function PaginationLi({children, ...restProps }) {
	return <Li {...restProps}>{children}</Li>;
};

Pagination.propTypes = {
	children: PropTypes.any
};