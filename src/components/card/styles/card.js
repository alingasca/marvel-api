import styled from 'styled-components/macro';
import cardBg from './../../../images/cardBg.jpg';
import favoriteBorder from './../../../images/icons/favoriteBorder.svg';
import favorite from './../../../images/icons/favorite.svg';
import expand from './../../../images/icons/expand.svg';
import saveImg from './../../../images/icons/save.svg';
import editImg from './../../../images/icons/edit.svg';
import closeImg from './../../../images/icons/close.svg';

export const CardContainer = styled.div`
    position: relative;
    width: 100%;
    max-width: 250px;
    height: 306px;
    background-color: grey;
    margin: 15px;
    transition: all .2s ease-in-out;
    border-radius: 15px;

    &:hover {
        transform: scale(1.1);
        z-index:999;
    }

    @media (min-width: 768px) {
        max-width: 250px;
        height: 306px;
    }
`;

export const Thumbnail = styled.div`
    width: 100%;
    background-color: #424242;
    border-radius: 5%;
`;

export const ImageFrame = styled.div`
    background: url(${cardBg}) top left / cover no-repeat;
    border-radius: 15px;
`;

export const Image = styled.img`
    -webkit-transform: scaleX(1);
    transform: scaleX(1);
    -webkit-transition: all .2s linear;
    transition: all .2s linear;
    overflow: hidden;
    -o-object-position: top center;
    object-position: top center;
    display: block;
    margin: 0;
    -o-object-fit: cover;
    object-fit: cover;
    -o-object-position: center center;
    object-position: center center;
    padding: 0;
    width: 100%;
    height: 198px;
    max-height: 198px;
    border-top-left-radius: 15px;
    border-top-right-radius: 15px;
`;

export const Content = styled.div`
   background-color: grey;
`;

export const ContentWrapper = styled.div`
    
`;

export const DescriptionWrapper = styled.div`
    margin-top:35px;
    display: none;
`;

export const Series = styled.div`
    text-align: left;
    padding-left: 20px;
`;

export const Events = styled.div`
   text-align: left;
   padding-left: 20px;
`;

export const WrapTitle = styled.div `
   
`;

export const Title = styled.h2`
    background-color: grey;
    padding: 8px;
    margin: 0px;
    margin-block-start: 0;
    margin-block-end: 0;
    font-size: 1em;
    display: block;
`;

export const NoData = styled.p`
    color: #fff;
`;

export const InputName = styled.input`
    height: 25px;
    width: 100%;
    max-width: 150px;
    display: none;
    margin-left: auto;
    margin-right: auto;
`;

export const SelectImage = styled.input`
    height: 25px;
    width: 100%;
    max-width: 150px;
    display: none;
    margin-left: auto;
    margin-right: auto;
    position: absolute;
    top: 50%;
    left: 50%;
    -ms-transform: translate(-50%,-50%);
    -webkit-transform: translate(-50%,-50%);
    -ms-transform: translate(-50%,-50%);
    transform: translate(-50%,-50%);
`;

export const Date = styled.time`
    background-color: grey;
    margin: 0px;
    padding: 8px;
    font-size: 0.7em;
    display: block;
`;

export const DescriptionTitle = styled.h3`
    display: block
    font-size: 1.7em;
    font-weight: 500;
    line-height: 1.3;
    margin-top: 25px;
    text-align: left;
    margin: 15px 0;
`;

export const Description = styled.p`
    color:#000;
    font-size: 1em;
    line-height:1.6;
    text-align: left;
    display: block;
    padding-left: 20px;
`;

export const Actions = styled.div`
    display: -webkit-box;
    display: -webkit-flex;
    display: -moz-box;
    display: -ms-flexbox;
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 8px;
    align-items: center;
    position: absolute;
    width: 100%;
    max-width: 234px;
    background-color: #333333;
    bottom: 0;
`;

export const ButtonAddToFavorites = styled.div`
    width: 28px;
    height: 28px;
    opacity: .5;
    cursor: pointer;
    
    background: url( ${({ x }) => (x !== true ? favoriteBorder : favorite) }) top left / cover no-repeat;

    &:hover {
        opacity: .9;
    }
`;

export const ButtonExpand = styled.div`
    width: 30px;
    height: 30px;
    opacity: .5;
    cursor: pointer;
    background: url(${expand}) top left / cover no-repeat;

    &:hover {
        opacity: .9;
    }
`;

export const ButtonEdit = styled.div`
    width: 30px;
    height: 30px;
    opacity: .5;
    cursor: pointer;
    background: url( ${({ active }) => (active === true ? saveImg : editImg) });
    position: fixed;
    bottom: 3%;
    right: 3%;
    display: none;

    &:hover {
        opacity: .9;
    }
`;

export const ButtonClose = styled.div`
    width: 30px;
    height: 30px;
    opacity: .5;
    cursor: pointer;
    background: url(${closeImg}) top left / cover no-repeat;

    display:${props => props.display ? 'block' : 'none'};

    &:hover {
        opacity: .9;
    }
`;