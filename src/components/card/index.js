/* eslint-disable no-unused-vars */
/* eslint-disable react/prop-types */
import React, { useContext, useState } from 'react';
import PropTypes from 'prop-types';
import { isImageBase64 } from './../../helpers/functions';
import { 
	CardContainer,
	Thumbnail,
	ImageFrame,
	Image,
	Content,
	ContentWrapper,
	DescriptionWrapper,
	DescriptionTitle,
	WrapTitle,
	Title,
	InputName,
	NoData,
	SelectImage,
	Date,
	Description,
	Events,
	Series,
	Actions,
	ButtonAddToFavorites,
	ButtonEdit,
	ButtonExpand,
	ButtonClose
} from './styles/card';
import GlobalContext from './../../context/global-context';

export default function Card({ children, ...restProps }) {
	return <CardContainer data-name='marvel-card' {...restProps}>{children}</CardContainer>;
}

Card.Thumbnail = function CardThumbnail({ ...restProps }) {
	return <Thumbnail className={'cardThumbnail'} {...restProps} />;
};

Card.ImageFrame = function CardImageFrame( { ...restProps }) {
	return <ImageFrame className={'cardImgFrame'} {...restProps} />;
};

Card.Image = function CardImage({ ...restProps }) {
	return <Image className={'cardImage'} {...restProps} />;
};

Card.Content = function CardContent({ ...restProps }) {
	return <Content className={'cardContent'} {...restProps} />;
};

Card.ContentWrapper = function CardContentWrapper({ ...restProps }) {
	return <ContentWrapper className={'contentWrapper'} {...restProps} />;
};

Card.WrapTitle = function CardWrapTitle({ children, ...restProps }) {
	return <WrapTitle className={'wrapTitle'} {...restProps} >{children}</WrapTitle>;
};

Card.Title = function CardTitle({ ...restProps }) {
	return <Title {...restProps} />;
};

Card.InputName = function CardInputName({ children, ...restProps }) {
	const [inputName, setInputName] = useState('');
	const context = useContext(GlobalContext);

	return <InputName placeholder="Change name" value={inputName} onChange={({ target }) => { setInputName(target.value); }} {...restProps} />;
};

Card.SelectImage = function CardSelectImage({children, ...restProps }) {

	const imageToBase64 = (target) => {
		const inputImg = target.previousSibling;
		let reader = new FileReader();

		reader.onload = function(){
			inputImg.src = reader.result;
		};

		if(target.files[0]){
			reader.readAsDataURL(target.files[0]);
		}
	};

	return <SelectImage value="" type="file" onChange={({ target }) => { imageToBase64(target); }} {...restProps} />;
};

Card.Date = function CardDate({ ...restProps }) {
	return <Date {...restProps} />;
};

Card.DescriptionWrapper = function CardDescriptionWrapper({ ...restProps }) {
	return <DescriptionWrapper className={'descriptionWrapper'} {...restProps} />;
};

Card.Description = function CardDescription({ ...restProps }) {
	return <Description className={'cardDescription'} {...restProps} />;
};

Card.DescriptionTitle = function CardDescriptionTitle({ ...restProps }) {
	return <DescriptionTitle {...restProps} ></DescriptionTitle>;
};

Card.NoData = function CardNoData({children, ...restProps }) {
	return <NoData {...restProps}>{children} </NoData>;
};

Card.Events = function CardEvents({ ...restProps }) {
	return <Events className={'descriptionWrapper'} {...restProps} />;
};

Card.Series = function CardSeries({ ...restProps }) {
	return <Series className={'descriptionWrapper'} {...restProps} />;
};

Card.Actions = function CardActions({ ...restProps }) {
	return <Actions className={'actions'} {...restProps} />;
};

Card.ButtonAddToFavorites = function CardButtonAddToFavorites({ ...restProps }) {
	const context = useContext(GlobalContext);
	
	const addToFavorites = (e) => {
		context.handleAddToFavorites(parseInt(e.target.parentElement.parentElement.parentElement.getAttribute('id')));
	};

	return <ButtonAddToFavorites className="addToFavorites" onClick={(e) => {addToFavorites(e);}} {...restProps} />;
};

Card.ButtonEdit = function CardButtonEdit({ ...restProps }) {
	const context = useContext(GlobalContext);
	const [editActive, setEditActive] = useState(false);

	const btnEdit = (e) => {
		setEditActive((editActive) => !editActive);
		const cardId =  context.modalIsOpen.cardId;
		const inputName = document.querySelector(`.inputName-${cardId}`);
		const inputImg = document.querySelector(`.selectImg-${cardId}`);
		const imgSrc = document.querySelector(`[data-base64="img-${cardId}"]`).src;
		const data = JSON.parse(localStorage.getItem('favorites'));
		let MatchedKeyToEdit = [];
		let restOfTheData = [];

		if (editActive === false) {
			inputName.style.display = 'block';
			inputImg.style.display = 'block';
		} else {
			data.map((item) => {
				if (item.id === parseInt(cardId)) {
					Object.keys(item).forEach(function(ele) {
						if (ele === 'name' && inputName.value !== '') {
							item[ele] = inputName.value
						}
						
						if (ele === 'thumbnail') {
							var thumbnail = {
								path: imgSrc,
								extension : false
							};
							item[ele] = thumbnail;
						}

					});
					return MatchedKeyToEdit.push(item );
				} else {
					return restOfTheData.push(item);
				}
			});

			let newFavoritesData = MatchedKeyToEdit.concat(restOfTheData);
			inputName.value = '';
			inputName.style.display = 'none';
			inputImg.style.display = 'none';
			localStorage.setItem('favorites', JSON.stringify(newFavoritesData));
		}
	
		context.handleDataSaved({ isSaved:true });
	};

	return <ButtonEdit active={editActive} className={'cardEdit'} onClick={(e) => { btnEdit(e);}} {...restProps} />;
};

Card.ButtonExpand = function CardButtonExpand({ ...restProps }) {
	const context = useContext(GlobalContext);

	const expand = (e) => {
		let element =  document.getElementById(e.target.parentElement.parentElement.parentElement.getAttribute('id'));
		console.log(element);
		const $body = document.querySelector('body');
		$body.style.position = 'fixed';
		element.classList.add('expand');
		context.handleModalIsOpen({isOpen:true, cardId: element.id});
	};

	return <ButtonExpand className={'expandCardModal'} onClick={(e) => { expand(e) }} {...restProps} />;
};

Card.ButtonClose = function CardButtonClose({ ...restProps }) {
	const context = useContext(GlobalContext);
	const [modalActive, setModalActive] = useState(false);
	
	const closeModal = (e) => {
		const element = document.getElementById(e.target.parentElement.getAttribute('id'));
		const $body = document.querySelector('body');
		element.classList.remove('expand');
		$body.style.position = '';

		setModalActive(false);
		context.handleModalIsOpen({isOpen:modalActive, cardId: undefined});
	};

	return <ButtonClose className={'closeCardModal'} onClick={(e) => closeModal(e)} {...restProps} />;
};

CardContainer.propTypes = {
	children: PropTypes.any
};