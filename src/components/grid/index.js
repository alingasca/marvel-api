import React from 'react';
import PropTypes from 'prop-types';
import { GridContainer } from './styles/grid';


export default function Grid({ children, ...restProps }) {
	return (
		<GridContainer {...restProps}>
			{children}
		</GridContainer>
	);
}

Grid.propTypes = {
	children: PropTypes.any
};