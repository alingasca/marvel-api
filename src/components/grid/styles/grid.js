import styled from 'styled-components/macro';

export const GridContainer = styled.div`
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
    -webkit-flex-direction: row;
    -ms-flex-direction: row;
    flex-direction: row;
    -webkit-flex-wrap: wrap;
    -ms-flex-wrap: wrap;
    flex-wrap: wrap;
    -webkit-box-pack: center;
    -webkit-justify-content: center;
    -ms-flex-pack: center;
    justify-content: center;
    margin-bottom: 0px;
    margin-top: 0px;
    padding: 0px 8px 60px;
    max-width: 1200px;
    margin: 0 auto;

    @media (min-width: 992px) {
        padding: 0px 20px 60px;
    }

    @media (min-width: 992px) {
        padding: 0px 30px 75px;
    }
`;