/* eslint-disable react/prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import { MainContent } from './styles/content';


export default function Content({ children, ...restProps }) {
	return (
		<MainContent {...restProps}>
			{children}
		</MainContent>
	);
}

Content.propTypes = {
	children: PropTypes.any
};