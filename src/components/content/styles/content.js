import styled from 'styled-components/macro';

export const MainContent = styled.div`
    display: flex;
    margin: 0px auto;
    padding: 0 15px;
    height: 100%;
    padding: 18px 0;
    justify-content: space-between;
    align-items: center;

    a {
        display: flex;
    }

    @media (min-width: 640px) {
        margin: 15px auto 60px;
        padding: 0 20px;
    }

    @media (min-width: 768px) {
        margin: 20px auto;
        padding: 0 20px;
    }

    @media (min-width: 768px) {
        margin: 30px auto;
        padding: 0 25px;
    }

    @media (min-width: 1200px) {
        margin: 40px auto 60px;
        padding: 0 30px;
    }
`;