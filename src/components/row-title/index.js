import React from 'react';
import PropTypes from 'prop-types';
import { RowTitleContainer, TitleH2 } from './styles/row-title';


export default function RowTitle({ children, ...restProps }) {
	return (
		<RowTitleContainer {...restProps}>
			{children}
		</RowTitleContainer>
	);
}

RowTitle.Title = function RowTitleTitle({ title, ...restProps }) {
	return <TitleH2 {...restProps}>{title}</TitleH2>;
};

RowTitle.propTypes = {
	children: PropTypes.any
};

RowTitle.Title.propTypes = {
	children: PropTypes.any,
	title:PropTypes.string.isRequired
};