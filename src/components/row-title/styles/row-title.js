import styled from 'styled-components/macro';

export const RowTitleContainer = styled.div`
  margin: 0px;
  height: auto;
  padding: 18px 0;
  width: 100%;
  margin: 35px auto 0;
  text-align: center;

  @media (max-width: 640px) {
    margin: 20px auto 0;
  }
`;

export const TitleH2 = styled.h2`
  color: #fff;
  height: 40px;
  font-size: 1.3em;
  width: 100%;
  margin: 0 auto;
`;
