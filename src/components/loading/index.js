/* eslint-disable react/prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import { LoadingContainer, LoadinImage} from './styles/loading';

export default function Loading({ children, ...restProps }) {
	return (
		<LoadingContainer {...restProps}>
			<LoadinImage />
			{children}
		</LoadingContainer>
	);
}

Loading.Image = function HeaderFrame({ children, ...restProps }) {
	return <LoadinImage {...restProps}>{children}</LoadinImage>;
};

Loading.propTypes = {
	children: PropTypes.any
};