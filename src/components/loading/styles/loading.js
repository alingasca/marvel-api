import styled from 'styled-components/macro';
import loadingIcon from './../../../images/icons/loading.svg';

export const LoadingContainer = styled.div`
  display: flex;
  margin: 0 56px;
  height: 80px;
  padding: 18px 0;
  justify-content: space-between;
  align-items: center;
  z-index: 2;

  a {
    display: flex;
  }

  @media (max-width: 1000px) {
    margin: 0 30px;
  }
`;

export const LoadinImage = styled.div`
  width: 100px;
  height: 100px;
  background: url(${loadingIcon}) center center / cover no-repeat;
  position: fixed;
  top: 45%;
  left: 50%;
  margin-left: -50px;

  @media (min-width: 768px) {
    width: 150px;
    height: 150px;
    margin-left: -75px;
  }

  @media (min-width: 1200px) {
    width: 180px;
    height: 180px;
    margin-left: -90px;
  }
`;