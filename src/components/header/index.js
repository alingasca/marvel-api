/* eslint-disable react/prop-types */
import React, { useState, useContext } from 'react';
import PropTypes from 'prop-types';
import { Link as ReachRouterLink } from 'react-router-dom';
import * as ROUTES from './../../constants/routes';
import GlobalContext from './../../context/global-context';
import { 
	Container,
	Background,
	Logo,
	PrimaryNavigation,
	Menu,
	MenuUl,
	MenuLi,
	SecondaryNavigation,
	Search,
	SearchIcon,
	SearchInput,
	MyFavorites,
	ButtonLink,
	Overlay
} from './styles';
import logo from '../../logo.svg';
import menu from '../../settings';
import searchImg from '../../images/icons/search.png';

export default function Header({ children, ...restProps }) {
	return (
		<Background data-testid="header-bg" {...restProps}>
			<Overlay />
			{children}
		</Background>
	);
}

Header.Frame = function HeaderFrame({ children, ...restProps }) {
	return <Container {...restProps}>{children}</Container>;
};

Header.PrimaryNavigation = function HeaderPrimaryNavigation({ children, ...restProps }) {
	const menuItems = menu.mainMenu;
	const categoryContext = useContext(GlobalContext);

	const listItems = menuItems.map((item) =>
		<MenuLi key={item} data-name={item.toLowerCase()} onClick={(e) => categoryContext.handleCategoryChange(e.target.attributes.getNamedItem('data-name').value.toLowerCase()) } >
			{item}
		</MenuLi>
	);

	return (
		<PrimaryNavigation className={'primaryNavigation'} {...restProps}>
			<Header.Logo to={ROUTES.HOME} src={logo} alt="Marvel App." />
			{children}
			<Menu>
				<MenuUl {...restProps}>{listItems}</MenuUl>
			</Menu>
		</PrimaryNavigation>
	);
};

Header.SecondaryNavigation = function HeaderSecondaryNavigation({ children, ...restProps }) {
	return <SecondaryNavigation {...restProps}>{children}</SecondaryNavigation>;
};

Header.Logo = function HeaderLogo({ to, ...restProps }) {
	return (
		<ReachRouterLink to={to}>
			<Logo {...restProps} />
		</ReachRouterLink>
	);
};

Header.Search = function HeaderSearch({...restProps }) {
	const [searchActive, setSearchActive] = useState(false);
	const [searchTerm, setSearchTerm] = useState('');
	const searchContext = useContext(GlobalContext);
  
	return (
		<Search {...restProps}>
			<SearchIcon onClick={() => setSearchActive((searchActive) => !searchActive)} data-id="search-click">
				<img src={searchImg} alt="Search" />
			</SearchIcon>
			<SearchInput
				value={searchTerm}
				onChange={({ target }) => {
					setSearchTerm(target.value);
					searchContext.handleSearchInput(target.value);
				}}
				placeholder='Search...'
				active={searchActive}
				data-testid="search-input"
			/>
		</Search>
	);
};

Header.MyFavorites = function HeaderMyFavorites({ children, ...restProps }) {
	return (
		<ReachRouterLink to={ROUTES.FAVORITES}>
			<MyFavorites {...restProps}>{children}</MyFavorites>
		</ReachRouterLink>
	);
};

Header.ButtonLink = function HeaderButtonLink({ children, ...restProps }) {
	return <ButtonLink {...restProps}>{children}</ButtonLink>;
};

Header.Overlay = function HeaderOverlay({ children, ...restProps }) {
	return <Overlay {...restProps}>{children}</Overlay>;
};

Header.propTypes = {
	children: PropTypes.any
};
Header.Overlay.propTypes = {
	children: PropTypes.any
};