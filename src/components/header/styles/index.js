import styled from 'styled-components/macro';
import { Link as ReachRouterLink } from 'react-router-dom';
import heartIcon from './../../../images/icons/favorite.svg';
import headerBg from './../../../images/misc/home-bg.jpg';

export const Container = styled.div`
    position: relative;
    display: flex;
    height: 100px;
    -webkit-box-pack: justify;
    justify-content: space-between;
    -webkit-box-align: start;
    align-items: start;
    z-index: 2;
    padding: 18px 0px;

  a {
    display: flex;
  }

  @media (max-width: 1200px) {
    margin: 0 30px;
  }

  @media (min-width: 640px) {
    align-items: center;
  }

  @media (min-width: 768px) {
    height: 80px;
    padding: 18px 0;
  }

  @media (min-width: 1200px) {
    padding: 18px 80px;
  }
`;

export const Overlay = styled.div`
  background-color: #000;
  opacity: 0.8;
  width:100%;
  height:100%;
  position: absolute;
  margin: 0;
`;

export const Background = styled.div `
    position: relative;
    display: flex;
    flex-direction: column;
    background: url(${headerBg}) top left / cover no-repeat;

    @media (max-width: 1100px) {
        ${({ dontShowOnSmallViewPort }) => dontShowOnSmallViewPort && 'background: none;'}
    }
`;

export const ButtonLink = styled(ReachRouterLink)`
  display: block;
  background-color: #e50914;
  width: 84px;
  height: fit-content;
  color: white;
  border: 0;
  font-size: 15px;
  border-radius: 3px;
  padding: 8px 17px;
  cursor: pointer;
  text-decoration: none;
  box-sizing: border-box;

  &:hover {
    background: #f40612;
  }
`;

export const Logo = styled.img`
  height: auto;
  width: 80px;
  margin-right: 15px;

  @media (min-width: 640px) {
    margin-right: 40px;
  }

  @media (min-width: 1449px) {
    width: 120px;
  }
`;

export const PrimaryNavigation = styled.div`
  display: flex;
`;

export const SecondaryNavigation = styled.div`
    display: -webkit-box;
    display: -webkit-flex;
    display: -ms-flexbox;
    display: flex;
`;

export const Menu = styled.nav`
    display: flex;
    align-items: center;

    @media (max-width: 640px) {
      position: absolute;
      width: 100%;
      bottom: 0;
      margin-bottom: 10px;
      text-align: center;
    }

`;

export const MenuUl = styled.ul`
    margin: 0;
    padding: 0;
    display: -webkit-box;
    display: -webkit-flex;
    display: -moz-box;
    display: -ms-flexbox;
    display: flex;
    -webkit-box-align: center;
    -webkit-align-items: center;
    -moz-box-align: center;
    -ms-flex-align: center;
    align-items: center;
    font-size: 1rem;

    text-align: center;
    margin: 0 auto;
`;

export const MenuLi = styled.li`
    color: #e5e5e5;
    list-style-type: none;
    margin-left: 18px;
    cursor: pointer;
    font-weight: ${({ active }) => (active === 'true' ? '700' : 'normal')};

    &:hover {
       color: #c5c4c4;
    }

    &:last-of-type {
        margin-right: 0;
    }

    @media (max-width: 640px) {
      margin: 18px;
      text-decoration: underline;

      &:last-of-type {
        margin-right: 18px;
    }
    }
`;

export const SearchInput = styled.input`
  background-color: #44444459;
  color: white;
  border: 1px solid white;
  transition: width 0.5s;
  height: 30px;
  font-size: 14px;
  margin-left: ${({ active }) => (active === true ? '10px' : '0')};
  padding: ${({ active }) => (active === true ? '0 10px' : '0')};
  opacity: ${({ active }) => (active === true ? '1' : '0')};
  width: ${({ active }) => (active === true ? '120px' : '0px')};
  outline: 0px;

  @media (min-width: 640px) {
    width: ${({ active }) => (active === true ? '200px' : '0px')};
  }
`;

export const Search = styled.div`
  display: flex;
  align-items: center;

  svg {
    color: white;
    cursor: pointer;
  }

  @media (max-width: 700px) {
    
  }
`;

export const SearchIcon = styled.button`
  cursor: pointer;
  background-color: transparent;
  border: 0;
  outline: 0px;

  img {
    filter: brightness(0) invert(1);
    width: 20px;
  }
`;

export const MyFavorites = styled.div`
    display: -webkit-box;
    display: -webkit-flex;
    display: -moz-box;
    display: -ms-flexbox;
    display: flex;
    flex-direction: column;
    background: url(${heartIcon}) top left / cover
    no-repeat;
    width: 24px;
    height: 24px;
    margin-top: 3px;
    margin-left: 15px;
    border: 0;
    outline: 0px;
    cursor: pointer; 
`;