
// Function to fetch the data from local storage
export const fetchDataFromLocalStorage = (searchInput, fetchedData, setSearchResults) => {
	let data = JSON.parse(localStorage.getItem('fetchedData')),
		searchResult = [],
		inputLength = searchInput.search.length,
		originalData = fetchedData;

	data.fetchedData.results.map((item) => {
		if (searchInput.search.toLowerCase() === item.name.substring(0, inputLength).toLowerCase() ) {
			return searchResult.push(item);
		}
		return false;
	});

	setSearchResults(searchResult);
	localStorage.setItem('searchResultsData', JSON.stringify(originalData.results = searchResult));
};

// Save favorites cards to local storage
export const saveFavoritesToLocal = (item) =>{
	let favorites;

	if (localStorage.getItem('favorites') === null) {
		favorites = [];
	} else {
		favorites = JSON.parse(localStorage.getItem('favorites'));
	}
	
	favorites.push(
		item
	);

	localStorage.setItem('favorites', JSON.stringify(favorites));
};

// Format date
export const formatDate = (date) => {
	var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
	var days = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];

	var d = new Date(date),
		month = '' + (months[d.getMonth()]),
		day = '' + d.getDate(),
		year = d.getFullYear(),
		// eslint-disable-next-line no-unused-vars
		weekDay = days[d.getDay()];

	if (month.length < 2) month = '0' + month;
	if (day.length < 2) day = '0' + day;

	return [day,' ',month,', ',year].join('');
};

export const capitalize = (string) => {
	if (typeof string !== 'string') return '';
	return string.charAt(0).toUpperCase() + string.slice(1);
};

// Check to see if the image is Base64
export const isImageBase64 = (data) => {
	let knownTypes = {
		'/': 'data:image/jpg;base64,',
		'i': 'data:image/png;base64,',
	};
      
	let image = new Image();
      
	if(!knownTypes[data[0]]){
		console.log('encoded image didn\'t match known types');
		return false;
	} else {
		image.src = knownTypes[0]+data;
		image.onload = function(){
			if(image.height === 0 || image.width === 0){
				console.log('encoded image missing width or height');
				return false;
			}
		};

		return true;
	} 
};

// Calculate page offset
export const calculatePageOffset = ( currentPage, itemsPerPage ) => {
	let offset = ( currentPage - 1) * itemsPerPage;
	return offset;
};