/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { fetchData } from './api/api';
import { fetchDataFromLocalStorage, saveFavoritesToLocal } from './helpers/functions';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Home, Favorites } from './pages';
import * as ROUTES from './constants/routes';
import GlobalContext from './context/global-context';
import useLocalStorage from './custom-hooks/index';

export default function App() {
	const [ fetchedData, setfetchedData ] = useState([]);
	const [ favoritesData, setFavoritesData ] = useState([]);
	const [ isLoaded, setIsLoaded ] = useState(false);
	const [ loadByCategory, setLoadByCategory ] = useState({ message: undefined });
	const [ searchInput, setSearchInput ] = useState({ input: '' });
	const [ pageOffSet, setPageOffSet ] = useState(1);
	const [ modalIsOpen, setModalIsOpen] = useState({isOpen:false, cardId: undefined});
	const [ addToFavorites, setAddToFavorites ] = useState();
	const [ changeName, setChangeName ] = useState('');
	const [ newCardImage, setNewCardImage ] = useState('');
	const [ savedData, setSavedData ] = useState({});

	//setSavedData
	const dataLoaded = () => setIsLoaded(true);
	const category = loadByCategory.message;

	// Use local storage
	const [dataInLocalStorage, setDataInLocalStorage] = useLocalStorage('fetchedData');
	const [searchResults, setSearchResults] = useLocalStorage('searchResultsData');
	
	// Fetch data from the API and store the data in a variable
	// Re-render whenever there is a change on setfetchedData or loadByCategory
	useEffect(() => {

		const fetchDataFromAPI = async () => {
			setfetchedData(await fetchData({ pageOffSet, category }));
			dataLoaded();
		};

		//Get the data from the local storage if the user is using the search feature.
		if (!searchInput.search && window.location.pathname !== '/favorites') {
			fetchDataFromAPI();
			localStorage.removeItem('searchResultsData');
			localStorage.removeItem('fetchedData');
		}

	}, [ setfetchedData, loadByCategory, searchInput, savedData, pageOffSet ]);

	useEffect(() => {
		if (searchInput.search) { 
			saveFetchedDataToLocalStorage();
			fetchDataFromLocalStorage(searchInput, fetchedData, setSearchResults);
		}
	}, [ searchInput ]);

	// Favorites
	useEffect(() => {
		saveFavorites();
	}, [ addToFavorites ]);

	//Add favorites to local storage
	const saveFavorites = () => {
		let data = fetchedData.results;

		if (data) {
			data.map((item) => {
				if (item.id === addToFavorites) {
					return saveFavoritesToLocal(item);
				}
				return false;
			});
		}
		setFavoritesData();
	};

	// Save Marvel data to local storage (To review this function)
	const saveFetchedDataToLocalStorage = () => {
		if (isLoaded) {
			return setDataInLocalStorage({fetchedData});
		}
	};

	return (
		<GlobalContext.Provider value={{ 
			fetchedData,
			favoritesData,
			isLoaded, 
			loadByCategory,
			searchInput,
			pageOffSet,
			modalIsOpen,
			addToFavorites,
			changeName,
			newCardImage,
			handleCategoryChange: (data) => setLoadByCategory({ message: data }),
			handleSearchInput: (data) => setSearchInput({ search: data }),
			handlePagination: (data) => setPageOffSet(data),
			handleModalIsOpen: (data) => setModalIsOpen(data),
			handleAddToFavorites: (data) => setAddToFavorites(data),
			handleChangeName: (data) => setChangeName(data),
			handleChangeCardImage: (data) => setNewCardImage(data),
			handleDataSaved: (data) => setSavedData(data),
		}}>
			<Router basename={window.location.pathname}>
				<Route IndexRoute path={ROUTES.HOME} exact component={Home} >
					<Home />
				</Route>
				<Route exact path={ROUTES.FAVORITES} component={Favorites}>
					<Favorites />
				</Route>
			</Router>
		</GlobalContext.Provider>
	);
}