/* eslint-disable array-callback-return */
import React, { useContext } from 'react';
import PropTypes from 'prop-types';
import { useLocation } from 'react-router-dom';
import GlobalContext from '../context/global-context';
import { Content, Grid, RowTitle, Card, Loading } from '../components';
import { formatDate, capitalize } from '../helpers/functions';
import { mainMenu } from '../settings.json';

export function ContentContainer({ children }) {
	const context = useContext(GlobalContext);
	const location = useLocation();
	const favoritesData = JSON.parse(localStorage.getItem('favorites'));
	let data = context.fetchedData.results;
	let pathLocation = 'index';

	if (location.pathname === '/favorites') {
		pathLocation = 'favorites';
		(favoritesData) ? data = favoritesData : data = 0;	
	}

	const renderDescription = (id, item, isModalOpen) => {
		if (isModalOpen === true && parseInt(id) === parseInt(item.id)) {
			return (
				<> {item.description ? <>
					<Card.DescriptionTitle>Description</Card.DescriptionTitle>
					<Card.Description>{item.description}</Card.Description></> :
					<><Card.DescriptionTitle>Description</Card.DescriptionTitle>
						<Card.Description>No description available</Card.Description></>
				}
				</>);
		}
	};

	const renderEvents = (id, item, isModalOpen) => {
		if (isModalOpen === true && parseInt(id) === parseInt(item.id)) {
			return (
				<>{(item.events.available > 0) ? <>
					<Card.DescriptionTitle>Events</Card.DescriptionTitle>
					{item.events.items.map((item) => (
						<Card.Events  key={item}>{item.name}</Card.Events>)) }</>:
					<><Card.DescriptionTitle>Events</Card.DescriptionTitle>
						<Card.Events  key={item}>No events available</Card.Events></>
				}
				</>);
		}
	};

	const renderSeries = (id, item, isModalOpen) => {
		if (isModalOpen === true && parseInt(id) === parseInt(item.id)) {
			return (
				<>{(item.events.available > 0) ? <>
					<Card.DescriptionTitle>Series</Card.DescriptionTitle>
					{item.events.items.map((item) => (
						<Card.Series  key={item}>{item.name}</Card.Series>)) }</>:
					<><Card.DescriptionTitle>Series</Card.DescriptionTitle>
						<Card.Series  key={item}>No series available</Card.Series>
					</>
				}
				</>);
		}
	};

	return (<>
		<RowTitle>
			{(context.loadByCategory.message === undefined ) ? 
				<RowTitle.Title title={mainMenu[0]} /> : 
				<RowTitle.Title title={capitalize(context.loadByCategory.message)}/>
			}	
		</RowTitle>
		
		<Content id={pathLocation}>
			{(!context.isLoaded ) ? <Loading /> : 
			<Grid>
			{(data) ? 
				data.map(item => (
					<Card key={item.id} id={item.id}>
						<Card.ButtonClose />
						<Card.Thumbnail>
							<Card.ImageFrame>
								<Card.Image data-base64={`img-${item.id}`} src={(item.thumbnail.extension === false) ? `${item.thumbnail.path}`:`${item.thumbnail.path}.${item.thumbnail.extension}`} alt={item.name}/>
								<Card.SelectImage className={`selectImg-${item.id}`} imageInput={`img-${item.id}`}/>
							</Card.ImageFrame>
							<Card.Content>
								<Card.ContentWrapper>
									<Card.WrapTitle>
										<Card.Title>{item.name}</Card.Title>
										<Card.InputName className={`inputName-${item.id}`}></Card.InputName>
										<Card.ButtonEdit />
									</Card.WrapTitle>
									<Card.Date>{formatDate(item.modified)}</Card.Date>
									<Card.DescriptionWrapper>
										{renderDescription(context.modalIsOpen.cardId, item, context.modalIsOpen.isOpen)}
										{renderEvents(context.modalIsOpen.cardId, item, context.modalIsOpen.isOpen)}
										{renderSeries(context.modalIsOpen.cardId, item, context.modalIsOpen.isOpen)}
									</Card.DescriptionWrapper>
								</Card.ContentWrapper>
							</Card.Content>
							<Card.Actions>
								<Card.ButtonAddToFavorites />
								<Card.ButtonExpand />
							</Card.Actions>
						</Card.Thumbnail>
					</Card>
				)) : <Card.NoData>Favourite list is empty</Card.NoData>
			}
			</Grid>
			}
			{children}
		</Content>
	</>
	);
}

ContentContainer.propTypes = {
	children: PropTypes.any
};