import React from 'react';
import PropTypes from 'prop-types';
import { Header } from '../components';

export function HeaderContainer({ children, ...restProps }) {
	return (
		<Header {...restProps}>
			<Header.Frame>
				<Header.PrimaryNavigation></Header.PrimaryNavigation>
				<Header.SecondaryNavigation>
					<Header.Search></Header.Search>
					<Header.MyFavorites></Header.MyFavorites>
				</Header.SecondaryNavigation>
			</Header.Frame>
			{children}
		</Header>
	);
}

HeaderContainer.propTypes = {
	children: PropTypes.any
};