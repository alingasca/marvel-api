import React, { useContext, useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import createPagination from './../helpers/pagination';
import { Pagination } from '../components';
import GlobalContext from './../context/global-context';
import settings from './../settings.json';

export function FooterContainer({ children, ...restProps }) {
	const context = useContext(GlobalContext);
	const [currentPage, setCurrentPage] = useState(1);

	useEffect(() => {
		context.handlePagination(currentPage);
	}, [ currentPage, context ]);

	const { pagination } = createPagination({
		numberOfArticles: settings.pagination.maxNumberOfArticles,
		articlesPerPage: settings.apiSettings.limit,
		numberOfButtons: settings.pagination.numberOfButtons,
		currentPage
	});

	const handleClick = page => setCurrentPage(page);

	return (
		<Pagination { ...restProps}>
			<Pagination.Frame>
				<Pagination.Ul>
					<Pagination.Li id="pgPrev" className={`${pagination[0] === currentPage && 'disabled'}`}
						onClick={handleClick.bind(null, currentPage - 1)}>Prev</Pagination.Li>

					{pagination.map((page, key) => ( 
						<Pagination.Li key={key} className={`${currentPage === page && 'active'}`}
							onClick={handleClick.bind(null, page)}>{page}</Pagination.Li>
					))}

					<Pagination.Li id="pgNext" className={`${pagination.reverse()[0] === currentPage && 'disabled'}`}
						onClick={handleClick.bind(null, currentPage + 1) } >Next</Pagination.Li>
				</Pagination.Ul>
			</Pagination.Frame>
			{children}
		</Pagination>
	);
}

FooterContainer.propTypes = {
	children: PropTypes.any
};