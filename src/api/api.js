import axios from 'axios';
import settings from './../settings.json';
import {calculatePageOffset} from './../helpers/functions';

export const fetchData = async ({ pageOffSet, category }) => {
	let categoryList = (category) ? category : 'characters';
	let	changeableUrl = `${settings.apiSettings.baseUrl}/${settings.apiSettings.version}/${settings.apiSettings.visibility}/${categoryList}?orderBy=${settings.apiSettings.orderBy}&limit=${settings.apiSettings.limit}&offset=${calculatePageOffset(pageOffSet, settings.apiSettings.limit)}&apikey=${settings.apiSettings.apiKey}`;

	try {
		const { data : { data: { results, offset, count, total, limit } } } = await axios(changeableUrl);
		return { results, offset, count, total, limit };
	} catch (error) {
		console.log('Fetch data error: ', error);
	}
};