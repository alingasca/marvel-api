## Developer name
Alin Gasca

## Application name
Marvel API

## Run NPM install
#### npm install

## Install dependencies
#### npm i -g eslint
#### npm install --save react-router-dom
#### npm install axios
#### npm install --save styled-components
#### npm install --save normalize.css

##  App settings
Before running the app, you must add your own private API keys provided by Marvel.com
https://developer.marvel.com/account

The settings file is located in the ./src directory and is called settings.json


"apiSettings": {
        "baseUrl": "https://gateway.marvel.com",
        "version": "v1",
        "visibility": "public",
        "orderBy": "name",
        "limit": 10,
        "apiKey": "xxxxxxxxxxxxxxxxxxx&hash=xxxxxxxxxxxxxxxxxxxx"
  },

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

